#include <GL/freeglut.h>
#include <iostream>
#include "Point.h"
#include "Button.h"

using namespace std;

Button redButton(-0.8f, 0.9f, 0.4f, 0.2f, 1.0f, 0.0f, 0.0f);
Button greenButton(-0.2f, 0.9f, 0.4f, 0.2f, 0.0f, 1.0f, 0.0f);
Button blueButton(0.4f, 0.9f, 0.4f, 0.2f, 0.0f, 0.0f, 1.0f);

Point points[1000];
int pointsCount = 0;

// Window width and height
int width = 400;
int height = 400;
float r = 0.0f;
float g = 0.0f;
float b = 0.0f;

// Convert window coordinates to Cartesian coordinates
void windowToScene(float& x, float& y) {
    x = (2.0f * (x / float(width))) - 1.0f;
    y = 1.0f - (2.0f * (y / float(height)));
}

void drawScene(){
    // Clear the screen and set it to current color (black)
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    // Objects to be drawn go here
    glEnable(GL_POINT_SMOOTH);
    glPointSize(15.0f);

    for (int i = 0; i < pointsCount; i++) {
        points[i].draw();
    }

    redButton.draw();
    greenButton.draw();
    blueButton.draw();

    // We have been drawing to the back buffer, put it in the front
    glutSwapBuffers();
}

void mouse(int button, int state, int x, int y) {
    /*
        button: 0 -> left mouse button
                2 -> right mouse button
        
        state:  0 -> mouse click
                1 -> mouse release
        
        x, y:   mouse location in window relative coordinates
    */

    float mx = x;
    float my = y;
    windowToScene(mx, my);

    if (button == 0 && state == 0) {
        if (redButton.isClicked(mx, my)) {
            r = 1.0f;
            g = 0.0f;
            b = 0.0f;
            cout << "Selected Color: Red" << endl;
        } else if (greenButton.isClicked(mx, my)) {
            r = 0.0f;
            g = 1.0f;
            b = 0.0f;
            cout << "Selected Color: Green" << endl;
        } else if (blueButton.isClicked(mx, my)) {
            r = 0.0f;
            g = 0.0f;
            b = 1.0f;
            cout << "Selected Color: Blue" << endl;
        } else {
            if (pointsCount < 1000) {
                points[pointsCount] = Point(mx, my, r, g, b);
                pointsCount++;
            }
        }

    }

    glutPostRedisplay();
}

void motion(int x, int y) {
    /*
        x, y:   mouse location in window relative coordinates
    */
    float mx = x;
    float my = y;
    windowToScene(mx, my);

    if (pointsCount < 1000) {
        points[pointsCount] = Point(mx, my, r, g, b);
        pointsCount++;
    }

    glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y) {
    /*
        key:    ASCII character of the keyboard key that was pressed
        x, y:   mouse location in window relative coordinates
    */
}

int main(int argc,char** argv) {
    // Perform some initialization
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH);
    glutInitWindowSize(width, height);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("GLUT App");

    // Set the Display Function
    glutDisplayFunc(drawScene);

    // Set the Mouse Function
    glutMouseFunc(mouse);

    // Set the Motion Function
    glutMotionFunc(motion);

    // Set the Keyboard Funcion
    glutKeyboardFunc(keyboard);

    // Run the program
    glutMainLoop();

    return 0;
}